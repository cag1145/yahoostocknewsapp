import yahoo_stock_news
from flask import Flask, render_template

app = Flask(__name__)


@app.route("/")
def home():
    query = "stock"  # Example query
    articles = get_stock_news(query)
    return render_template("index.html", articles=articles)


if __name__ == "__main__":
    app.run(debug=True)
