import requests
from flask import Flask

app = Flask(__name__)


def get_stock_news(stock_name):
    url = "https://yh-finance.p.rapidapi.com/auto-complete"
    querystring = {"q": stock_name, "region": "US"}
    headers = {
        "X-RapidAPI-Key": "295e147550msh296cc12e7ce5013p1cb9c3jsn3931cc8d36e6",
        "X-RapidAPI-Host": "yh-finance.p.rapidapi.com",
    }
    response = requests.get(url, headers=headers, params=querystring)
    return response.json()
